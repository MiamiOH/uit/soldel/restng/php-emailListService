<?php

namespace MiamiOH\PhpEmailListService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class EmailResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'EmailList.Email',
            'type' => 'object',
            'properties' => array(
                'uniqueId' => array(
                    'type' => 'string',
                ),
                'emailAddress' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'EmailList.Email.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/EmailList.Email'
            )
        ));

        $this->addDefinition(array(
            'name' => 'EmailList.Email.Update.Response',
            'type' => 'object',
            'properties' => array(
                'importId' => array(
                    'type' => 'integer',
                ),
                'accountType' => array(
                    'type' => 'string',
                ),
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'EmailREST',
            'class' => 'MiamiOH\PhpEmailListService\Services\EmailREST',
            'description' => 'Update the Employee email list',
            'set' => array(
                'email' => array('type' => 'service', 'name' => 'EmaiList.Email'),

            ),
        ));

        $this->addService(array(
            'name' => 'EmaiList.Email',
            'class' => 'MiamiOH\PhpEmailListService\Services\Email',
            'description' => 'Update the Employee email list',
            'set' => array(
                'emailHelper' => array('type' => 'service', 'name' => 'EmailHelper'),

            ),
        ));

        $this->addService(array(
            'name' => 'EmailHelper',
            'class' => 'MiamiOH\PhpEmailListService\Services\EmailHelper',
            'description' => 'Provides the parameterized object of type Emma',
            'set' => array(
                'dataSourceFactory' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
            ),
        ));

        $this->addService(array(
            'name' => 'EmailResponseREST',
            'class' => 'MiamiOH\PhpEmailListService\Services\EmailResponseREST',
            'description' => 'Provides the required response from Emma',
            'set' => array(
                'emailResponse' => array('type' => 'service', 'name' => 'EmailResponse'),
            ),
        ));

        $this->addService(array(
            'name' => 'EmailResponse',
            'class' => 'MiamiOH\PhpEmailListService\Services\EmailResponse',
            'description' => 'Provides the required response from Emma',
            'set' => array(
                'emailHelper' => array('type' => 'service', 'name' => 'EmailHelper'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'update',
            'name' => 'EmailList.email.v1',
            'description' => 'EmailList email resource',
            'tags' => array('EmailList'),
            'pattern' => '/emailList/email/v1',
            'service' => 'EmailREST',
            'method' => 'updateEmailREST',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array('application' => 'WebServices',
                        'module' => 'EmailList',
                        'key' => 'update'
                    ),
                ),
            ),
            'options' => array(
                'accountType' => array(
                    'required' => true,
                    'description' => 'Group label of the emailList. Acceptable values are configured in CAM (keys of the config items under the AccountDetails category). E.g. MU-employees'),
            ),
            'body' => array(
                'description' => 'A list of emails to be added to the group',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/EmailList.Email.Collection'
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'An import ID and Account type.',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/EmailList.Email.Update.Response',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'EmailList.email.v1.read',
            'description' => 'EmailList email resource',
            'tags' => array('EmailList'),
            'pattern' => '/emailList/email/v1',
            'service' => 'EmailResponseREST',
            'method' => 'getEmailResponseREST',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array('application' => 'WebServices',
                        'module' => 'EmailList',
                        'key' => 'update'
                    ),
                ),
            ),
            'options' => array(
                'importID' => array(
                    'description' => 'Import id from the Emma update service',
                    'required' => true,
                ),
                'accountType' => array(
                    'required' => true,
                    'description' => 'Group label of the emailList. Acceptable values are configured in CAM (keys of the config items under the AccountDetails category). E.g. MU-employees'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list member status affected by import id',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/EmailList.Email.Collection',
                    )
                ),
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}