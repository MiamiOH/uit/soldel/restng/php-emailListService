<?php

namespace MiamiOH\PhpEmailListService\Services;


class EmailResponseREST extends \MiamiOH\RESTng\Service
{
    /** @var \MiamiOH\PhpEmailListService\Services\EmailResponse
     * $emailResponse
     */
    private $emailResponse;

    public function setEmailResponse($emailResponse)
    {
        /** @var \MiamiOH\PhpEmailListService\Services\Email
         * $email
         */
        $this->emailResponse = $emailResponse;
    }

    public function getEmailResponseREST()
    {
        //log
        $this->log->debug('Start the email Response service.');

        $payload = [];
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();


        try {

            if (!(isset($options['importID']) && $options['importID'])) {
                throw new \Exception("Please enter import ID");
            }
            if (!(isset($options['accountType']) && $options['accountType'])) {
                throw new \Exception("Please enter account Type");
            }
            $result = $this->emailResponse->getEmmaEmailResponse($options);
            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $payload['message'] = "addedList: List of emails added, errorList: List of emails failed to add";
            $payload['emmaResponse'] = $result;

        } catch (\Exception $e) {

            $this->log->error($e->getMessage());
            $payload['message'] = $e->getMessage();
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
        }

        $response->setPayload($payload);
        return $response;
    }
}
