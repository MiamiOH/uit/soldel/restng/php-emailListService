<?php

namespace MiamiOH\PhpEmailListService\Services;

class EmailREST extends \MiamiOH\RESTng\Service
{
    /** @var \MiamiOH\PhpEmailListService\Services\Email
     * $email
     */
    private $email;

    public function setEmail($email)
    {
        /** @var \MiamiOH\PhpEmailListService\Services\Email
         * $email
         */
        $this->email = $email;
    }

    public function updateEmailREST()
    {
        //log
        $this->log->debug('Start the email service.');

        $payload = [];
        $request = $this->getRequest();
        $response = $this->getResponse();

        $emailList = $request->getData();
        $options = $request->getOptions();

        try {

            if (!(isset($options['accountType']) && $options['accountType'])) {
                throw new \Exception("Please choose account type to post");
            }

            $result = $this->email->updateEmailList($emailList, $options['accountType']);

            $payload['message'] = "Email list posted successfully";
            $payload['emmaImportID'] = $result['import_id'];
            $payload['accountType'] = $options['accountType'];
            $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        } catch (\Exception $e) {
            $this->log->error($e->getMessage());
            $payload['message'] = $e->getMessage();
            $payload['emmaImportID'] = null;
            $payload['accountType'] = $options['accountType'];
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
        }

        $response->setPayload($payload);
        return $response;
    }
}