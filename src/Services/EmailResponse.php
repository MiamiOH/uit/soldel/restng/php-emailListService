<?php

namespace MiamiOH\PhpEmailListService\Services;

use MiamiOH\RESTng\App;

class EmailResponse extends \MiamiOH\RESTng\Service
{
    private $emma;
    private $emailHelper;


    /**
     * @param $emailHelper
     */
    public function setEmailHelper($emailHelper)
    {

        $this->emailHelper = $emailHelper;

    }

    public function getEmmaEmailResponse($options)
    {

        $this->emma = $this->emailHelper->getInstance($options['accountType']);

        // Check for import statistics
        $importStatistics = $this->emma->membersImportStats($options['importID']);
        $importStatisticsJson = json_decode($importStatistics, true);

        // If we receive timestamp of import finished, then get the member details
        // otherwise throw an exception that import processing is not finished
        if ((array_key_exists('import_finished', $importStatisticsJson)) && ($importStatisticsJson['import_finished'] != null)) {

            $importResult = $this->emma->membersImported($options['importID']);

            $importResultArray = json_decode($importResult, true);

            $resultArray = array(
                'addedList' => array(),
                'errorList' => array(),
            );
            // If member_status_id is e indicates that this record is added to Error list in Emma
            // as error data has been posted. member_status_id is a indicates that record is added active list.
            if (count($importResultArray) >= 1) {

                foreach ($importResultArray as $tempResult) {
                    if ($tempResult['member_status_id'] == 'e') {
                        $resultArray['errorList'][] = $tempResult;
                    } else {
                        if ($tempResult['member_status_id'] == 'a') {
                            $resultArray['addedList'][] = $tempResult;
                        }
                    }

                }
            }

        } else {
            throw new \Exception("Please Retry. Import processing is not finished yet");
        }

        return $resultArray;
    }

}