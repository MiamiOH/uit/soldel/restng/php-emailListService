<?php

namespace MiamiOH\PhpEmailListService\Services;

use MiamiOH\RESTng\Connector\DataSource;
use MiamiOH\RESTng\Connector\DataSourceFactory;

class EmailHelper extends \MiamiOH\RESTng\Service
{
    private $account_id;
    private $public_key;
    private $private_key;
    private $groupIDs;
    private $dsFactory;

    private $applicationName = 'EmailList';
    private $categoryList = [

        'AccountDetails' => 'AccountDetails',
    ];

    public function setDataSourceFactory($dsFactory)
    {

        $this->dsFactory = $dsFactory;
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    public function getInstance($accountType)
    {
        $accountDetails = $this->getConfiguration($accountType);
        $array = explode(':', $accountDetails);
        $this->account_id = $array[0];
        // explode creates array of strings.
        // So using array_map to convert it to array of integers as group ids should be array of integers.
        $this->groupIDs = array_map('intval', explode(',', $array[1]));
        /**
         * @var DataSource $dataSource
         * */
        $dataSource = $this->dsFactory->getDataSource('EMAIL_WS_USER');
        $this->public_key = $dataSource->getUser();
        $this->private_key = $dataSource->getPassword();

        $instance = new Emma($this->account_id, $this->public_key, $this->private_key);
        return $instance;
    }

    public function getConfiguration($accountType)
    {

        $payload = $this->configuration->getConfiguration($this->applicationName, $this->categoryList['AccountDetails']);

        return $payload[$accountType];

    }

    public function getGroupIds()
    {
        return $this->groupIDs;
    }
}