<?php

namespace MiamiOH\PhpEmailListService\Services;

class Email extends \MiamiOH\RESTng\Service
{
    private $emailHelper;
    private $emma;

    /**
     * @param $emailHelper
     */
    public function setEmailHelper($emailHelper)
    {

        $this->emailHelper = $emailHelper;

    }

    /**
     * @param $emailList
     */
    public function updateEmailList($emailList, $accountType)
    {
        $params = array();


        $this->emma = $this->emailHelper->getInstance($accountType);

        $groupIds = $this->emailHelper->getGroupIds();

        // Construct parameters for Emma function call
        foreach ($emailList as $email) {
            $params['members'][]["email"] = $email['emailAddress'];

        }

        $params['add_only'] = true;
        $params['group_ids'] = $groupIds;


        // Add new members or update existing members in bulk to group IDs in Emma
        $jsonResult = $this->emma->membersBatchAdd($params);

        $result = json_decode($jsonResult, true);
        return $result;

    }

}