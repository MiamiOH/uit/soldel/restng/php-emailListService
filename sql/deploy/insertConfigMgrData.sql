set define off;

delete from cm_config where config_application = 'EmailList';

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('EmailList','Test',
        'text', 'scalar', 'AccountDetails',
        'account id and group id for posting data to emma interface',
        '1777284:2043524,2042500,2044548',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('EmailList','UserEmail',
        'text', 'scalar', 'UserDetails',
        'Email id for which the response has to send ',
        'pashaktm@miamioh.edu',
        sysdate, 'doej');
