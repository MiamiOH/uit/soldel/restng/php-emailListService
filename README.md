# RESTng integration

export EMAIL_HOME=/vagrant/projects/php-emailListService

Please noted that php-emailListService has been converted to RESTng 2.0. Afte conversion is done, PHPunit has been exectued.

# Integrate with RESTng
# One time for new RESTng install
ln -s $EMAIL_HOME/rest/src/RESTconfig $RESTNG_HOME/RESTng/RESTconfig/emailList
ln -s $EMAIL_HOME/rest/src/Service $RESTNG_HOME/RESTng/Service/EmailList

We are using the EmmaPHP api wrapper classes from https://github.com/myemma/EmmaPHP.
We have copied files under src to our Service class as our build process does not support submodules.


Run these
cd $EMAIL_HOME/sql/dev
exit | /usr/lib/oracle/12.1/client64/bin/sqlplus restng/Hello123@//db:1521/XE @setupAuth.sql
cd $EMAIL_HOME/sql/deploy
exit | /usr/lib/oracle/12.1/client64/bin/sqlplus restng/Hello123@//db:1521/XE @insertConfigMgrData.sql

# Update script

## Additional Perl modules
#for vagrant users run  in vagrant test

sudo perl -MCPAN -e 'install Net::LDAPS'

## Config file

Create a config directory with a file named something like local.conf, contents:

log_dir = /vagrant/projects/php-emailListService/logs
log = emma_put_process_log.txt
responseLog = emma_get_process_log.txt
log_history = 5
server = http://ws/api
username = EMAIL_WS_USER
password = email
ldapServer = ldapt.muohio.edu
ldapBase = ou=people,dc=muohio,dc=edu
ldapBindDN = uid=php-ldap,ou=ldapids,dc=muohio,dc=edu
ldapBindPasswd = enter password
accountType = Test
output_dir = /vagrant/projects/php-emailListService/run
output_file = import.txt
input_dir = /vagrant/projects/php-emailListService/run
input_file = import.txt

## Running the script
# Below script gets email list from Ldap and send PUT request to Email REST service.
# Response(list of import Ids) are written into output file.
bin/updateMailList.pl --config config/local.conf

# Below script reads Import ID list from previous script output file and creates an hash of accountType and response
# to send notification to product owner.
bin/getMailListResponse.pl --config config/local.conf



# Create datasource for RESTng
# DataSource file location
$RESTNG_HOME/conf/DataSources.xml

# One time for new RESTng install. User and password is used to connect to test account in Emma.
<datasource>
  <name>EMAIL_WS_USER</name>
  <type>OCI8</type>
  <user>shettycm@miamioh.edu</user>
  <password>Whilesprint123</password>
  <host>http://ws/api</host>
  <database>XE</database>
  <port></port>
</datasource>

## Authorizations

Access is granted to the ConfigMgr settings by, and notifications are sent to, the group:

DN:CN=ucm-emaillistadm,OU=ucm,OU=pre,OU=manual,OU=Groups,DC=it,DC=muohio,DC=edu
E-Mail:ucm-emaillistadm@miamioh.edu

## Appworx Jobs

1. Update Email List:
- Job name: UPDATE_EMMA_EMAIL_LIST
- Schedule: 6 PM daily 
- Description: It runs first to fetch the email ids of faculty and staff from LDAP and imports it to Emma

2. Get Email List Response:
- Job name: EMMA_GET_MAIL_RESPONSE
- Schedule: 6 AM daily
- Description: It gets the response of imports from Emma and send email to the UCM group