Feature: Get Email Response
  As a developer using the this API
  can get the response for email import id

  Background:

  Scenario: Get the response for Import ID
    Given a REST client
    And a token for the EMAIL_WS_USER user
    When I give the HTTP Content-type header with the value "application/json"
    And I make a GET request for /emailList/email/v1?importID=3008132&accountType=Test
    Then the HTTP status code is 200

