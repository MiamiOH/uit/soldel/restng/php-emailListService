Feature: Update Email list
  As a developer using the Email API
  can post the list of emails to Emma

  Background:

  Scenario: Update the email list in emma
    Given a REST client
    And a token for the EMAIL_WS_USER user
    And a collection of email records to update
    When I give the HTTP Content-type header with the value "application/json"
    And I make a PUT request for /emailList/email/v1?accountType=Test
    Then the HTTP status code is 200

