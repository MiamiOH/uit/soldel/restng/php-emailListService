#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use LWP::UserAgent;
use JSON;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given qr/a collection of email records to update/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                 'uniqueId' => '123456',
                 'emailAddress' => 'doej@miamioh.edu',
            },
        ];

};
