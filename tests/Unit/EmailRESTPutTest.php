<?php

namespace MiamiOH\PhpEmailListService\Tests\Unit;

class EmailRESTPutTest extends \MiamiOH\RESTng\Testing\TestCase
{
    /*************************/
    /**********Set Up*********/
    /*************************/
    private $emailREST;

    private $api;
    private $email;


    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp(): void
    {
        //set up the mock api:
        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->email = $this->getMockBuilder('\MiamiOH\PhpEmailListService\Services\Email')
            ->setMethods(array('updateEmailList'))
            ->getMock();

        $this->email->method('updateEmailList')
            ->willReturn(['import_id' => 123]);

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData', 'getOptions'))
            ->getMock();

        $this->request->method('getOptions')
            ->willReturn(['accountType' => 'Test']);

        $this->request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'updateDataMock')));

        //set up the service with the mocked out resources:
        $this->emailREST = new \MiamiOH\PhpEmailListService\Services\EmailREST();
        $this->emailREST->setLogger();
        $this->emailREST->setEmail($this->email);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testUpdateEmailREST()
    {
        $this->mockModel = [
            [
                'uniqueID' => 123456,
                'emailAddress' => 'shettycm@miamioh.edu',
            ]
        ];

        $this->emailREST->setRequest($this->request);

        $response = $this->emailREST->updateEmailREST();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());

    }

    public function updateWith($subject)
    {
        $this->createModel = $subject;
        return true;
    }

    public function updateDataMock()
    {
        return $this->mockModel;
    }
}