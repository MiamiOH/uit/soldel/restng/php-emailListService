<?php

namespace MiamiOH\PhpEmailListService\Tests\Unit;

class EmailPutTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $email;
    private $emailHelper;
    private $emma;
    private $params = [];
    private $request;

    protected function setUp(): void
    {
        $this->params = [];
        $this->emailHelper = $this->getMockBuilder('\MiamiOH\PhpEmailListService\Services\EmailHelper')
            ->setMethods(array('getGroupIds', 'getInstance'))
            ->getMock();
        $this->emma = $this->getMockBuilder('\MiamiOH\PhpEmailListService\Services\Emma')
            ->disableOriginalConstructor()
            ->setMethods(array('membersBatchAdd'))
            ->getMock();

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $this->emailHelper->method('getInstance')
            ->willReturn($this->emma);

        $this->email = new \MiamiOH\PhpEmailListService\Services\Email();

        $this->email->setEmailHelper($this->emailHelper);
    }

    public function testUpdateEmaillist()
    {
        $emailList = [
            [
                'uniqueId' => 'doej',
                'emailAddress' => 'doej@miamioh.edu',
            ]
        ];

        $options = [
            'type' => 'Test'
        ];

        $this->params = [
            'import_id' => 3078788
        ];


        $this->emma->method('membersBatchAdd')
            ->will($this->returncallback(array($this, 'mockMembersBatchAdd')));

        $response = $this->email->updateEmailList($emailList, $options);


        $this->assertEquals($response['import_id'], 3078788);


    }

    public function mockMembersBatchAdd()
    {
        return json_encode($this->params);
    }
}