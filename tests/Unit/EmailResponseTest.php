<?php

namespace MiamiOH\PhpEmailListService\Tests\Unit;

class EmailResponseTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $emailResponse;
    private $emailHelper;
    private $emma;
    private $params = [];
    private $mockImportStats = [];
    private $request;

    protected function setUp():void
    {
        $this->params = [];
        $this->emailHelper = $this->getMockBuilder('\MiamiOH\PhpEmailListService\Services\EmailHelper')
            ->setMethods(array('getInstance'))
            ->getMock();

        $this->emma = $this->getMockBuilder('\MiamiOH\PhpEmailListService\Services\Emma')
            ->disableOriginalConstructor()
            ->setMethods(array('membersImported', 'membersImportStats'))
            ->getMock();

        $this->emailHelper->method('getInstance')
            ->willReturn($this->emma);


        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();


        $this->emailResponse = new \MiamiOH\PhpEmailListService\Services\EmailResponse();
        $this->emailResponse->setLogger();
        $this->emailResponse->setEmailHelper($this->emailHelper);
    }

    public function testGetEmmaEmailResponse()
    {

        $options = [
            'accountType' => 'Test',
            'importID' => '1234123'
        ];

        $this->params = [
            [
                'member_id' => 766429828,
                'change_type' => 'a',
                'member_status_id' => 'a',
                'email' => null
            ]
        ];
        $this->mockImportStats = [
            'import_finished' => "20160919"

        ];
        $this->emma->method('membersImportStats')
            ->with(1234123)
            ->will($this->returnCallback(array($this, 'mockMembersImportStats')));

        $this->emma->method('membersImported')
            ->will($this->returnCallback(array($this, 'mockMembersImported')));


        $response = $this->emailResponse->getEmmaEmailResponse($options);

        $this->assertEquals(count($response), 2);
        $this->assertEquals($response['addedList'][0]['member_id'], 766429828);

    }

    public function membersImportedWithParams()
    {
        return true;
    }

    public function mockMembersImported()
    {

        return json_encode($this->params);
    }

    public function mockMembersImportStats()
    {
        return json_encode($this->mockImportStats);
    }
}