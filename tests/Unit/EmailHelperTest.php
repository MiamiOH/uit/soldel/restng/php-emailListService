<?php

namespace MiamiOH\PhpEmailListService\Tests\Unit;

use MiamiOH\RESTng\Connector\DataSource;


class EmailHelperTest extends \MiamiOH\RESTng\Testing\TestCase
{
    /*************************/
    /**********Set Up*********/
    /*************************/
    private $emailHelper;
    public $options;
    private $dsFactoryObj;
    private $mockDataSource = '';
    private $mockConfiguration = '';
    private $configuration;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp(): void
    {
        $this->dsFactoryObj = $this->getMockBuilder('\MiamiOH\RESTng\Connector\DataSourceFactory')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        $this->configuration = $this->getMockBuilder('\MiamiOH\RESTng\Util\Configuration')
            ->setMethods(array('getConfiguration'))
            ->getMock();


        $this->dsFactoryObj->method('getDataSource')
            ->will($this->returnCallback(array($this, 'getDataSourceMock')));

        //set up the service with the mocked out resources:
        $this->emailHelper = new \MiamiOH\PhpEmailListService\Services\EmailHelper();
        $this->emailHelper->setLogger();
        $this->emailHelper->setDataSourceFactory($this->dsFactoryObj);
        $this->emailHelper->setConfiguration($this->configuration);
    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testGetInstance()
    {
        $options = 'Test';

        $this->mockDataSource = DataSource::fromArray(
            [
                'name' => 'EMAIL_WS_USER',
                'type' => 'Other',
                'user' => 'shettycm@miamioh.edu',
                'password' => 'Whilesprint123'
            ]
        );

        $this->dsFactoryObj->method('getDataSource')
            ->willReturn($this->mockDataSource);

        $this->mockConfiguration = array('Test' => '1777284:2043524,2042500,2044548');

        $this->configuration->method('getConfiguration')
            ->willReturn($this->mockConfiguration);

        $instance = $this->emailHelper->getInstance($options);
        $this->assertEquals($instance->base_url, "https://api.e2ma.net/");

    }

    public function getDataSourceMock()
    {
        return $this->mockDataSource;
    }

    public function getConfigurationMock()
    {
        return $this->mockConfiguration;
    }


}
