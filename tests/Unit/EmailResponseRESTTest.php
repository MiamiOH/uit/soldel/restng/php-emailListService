<?php

namespace MiamiOH\PhpEmailListService\Tests\Unit;

class EmailResponseRESTTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $emailResponseREST;

    private $api;

    private $emailResponse;

    private $request;

    protected function setUp(): void
    {
        //set up the mock api:
        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->emailResponse = $this->getMockBuilder('\MiamiOH\PhpEmailListService\Services\Email')
            ->setMethods(array('getEmmaEmailResponse'))
            ->getMock();


        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $this->request->method('getOptions')
            ->willReturn(['accountType' => 'Test', 'importID' => 1234123]);


        //set up the service with the mocked out resources:
        $this->emailResponseREST = new \MiamiOH\PhpEmailListService\Services\EmailResponseREST();
        $this->emailResponseREST->setLogger();
        $this->emailResponseREST->setEmailResponse($this->emailResponse);

    }

    public function testGetEmailResponseREST()
    {


        $this->emailResponseREST->setRequest($this->request);

        $response = $this->emailResponseREST->getEmailResponseREST();

        $payload = $response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals($payload['message'], "addedList: List of emails added, errorList: List of emails failed to add");

    }

}
