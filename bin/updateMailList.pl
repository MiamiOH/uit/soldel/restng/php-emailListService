#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

use Net::LDAPS;
use Net::LDAP::Control::Paged;
use Net::LDAP::Constant qw(LDAP_CONTROL_PAGED);
use LWP::UserAgent;
use JSON;
use Data::Dumper;

# 1. Declare our configuration variables, but do not define them. We
#    will do a defined() test later.
my $logPath;
my $logFileName;
my $keepLogs;
my $server;
my $username;
my $password;
my $ldapServer;
my $ldapBase;
my $ldapBindDN;
my $ldapBindPasswd;
my $outputPath;
my $outputFileName;

my $configFile = '';
my $VERBOSE = 0;
my $help = 0;

# 2. Read any options provided on the command line into our configuration
#    variables. This will cause those provided to be defined.
my $result = GetOptions(
    'log_dir=s' => \$logPath,
    'log=s' => \$logFileName,
    'log_history=s' => \$keepLogs,
    'config=s' => \$configFile,
    'verbose' => \$VERBOSE,
    'help' => \$help,
    'output_dir=s' => \$outputPath,
    'output_file=s' => \$outputFileName
);

# 3. Document the command line options (and by extension the config file).
if ($help) {
    print <<EOP;

Call notification service for a list of recipients

Options:

           log_dir = path to log file
               log = log file name
       log_history = number of log files to keep
            config = path to config file
           verbose = verbose output
              help = display this help
        output_dir = path to output file
       output_file = output file name

EOP
}

# 4. Read the config file if provided.
our %config;
if (-e $configFile) {
    open CONFIG, $configFile or die "Couldn't open $configFile: $!";
    while (<CONFIG>) {
        chomp;
        next unless ($_);
        my ($key, $value) = split(/\s*=\s*/, $_, 2);
        $config{$key} = $value;
    }
    close CONFIG;
}

# 5. Set up configuration from the config file, allowing override by
#    command line options if provided.
$logPath = $config{'log_dir'}
    if (!defined($logPath) && exists($config{'log_dir'}));
$logFileName = $config{'log'}
    if (!defined($logFileName) && exists($config{'log'}));
$keepLogs = $config{'log_history'}
    if (!defined($keepLogs) && exists($config{'log_history'}));
$server = $config{'server'}
    if (!defined($server) && exists($config{'server'}));
$username = $config{'username'}
    if (exists($config{'username'}));
$password = $config{'password'}
    if (exists($config{'password'}));
$ldapServer = $config{'ldapServer'}
    if (exists($config{'ldapServer'}));
$ldapBase = $config{'ldapBase'}
    if (exists($config{'ldapBase'}));
$ldapBindDN = $config{'ldapBindDN'}
    if (exists($config{'ldapBindDN'}));
$ldapBindPasswd = $config{'ldapBindPasswd'}
    if (exists($config{'ldapBindPasswd'}));
$outputPath = $config{'output_dir'}
    if (!defined($outputPath) && exists($config{'output_dir'}));
$outputFileName = $config{'output_file'}
    if (!defined($outputFileName) && exists($config{'output_file'}));

# 6. Apply default configuration for any missing items.
$logPath = '/var/logs' unless (defined($logPath));
$logFileName = 'Emma_email_notification_log.txt' unless (defined($logFileName));
$keepLogs = 0 unless (defined($keepLogs));
$server = '' unless (defined($server));
$username = '' unless (defined($username));
$password = '' unless (defined($password));
$outputPath = '/var/logs' unless (defined($outputPath));
$outputFileName = 'output.txt' unless (defined($outputFileName));

unless ($server) {
    print "No valid Miami server provided in command line or config file\n";
    exit 1;
}

unless ($username) {
    print "No valid Miami username provided in command line or config file\n";
    exit 1;
}

unless ($password) {
    print "No valid Miami password provided in command line or config file\n";
    exit 1;
}


unless (-e $logPath) {
    my $r = `mkdir -p $logPath`;
    die $r if ($r);
}

# Handle any shell expansion, such as ~, in the path
my $realLogPath = `cd $logPath;pwd`;
chomp $realLogPath;

our $logFile = $realLogPath . '/' . $logFileName;

# Remove old logs
opendir(LOGDIR, $realLogPath) or die "Couldn't open $realLogPath: $!";
while (my $file = readdir(LOGDIR)) {
    next unless ($file =~ /^$logFileName\.(\d+)/);
    unlink($realLogPath . '/' . $file) if ($1 >= $keepLogs);
}
closedir(LOGDIR);

if ($keepLogs) {
    # Increment exist logs
    for (my $i = $keepLogs; $i >= 1; $i--) {
        my $newNumber = $i + 1;
        rename($logFile . '.' . $i, $logFile . '.' . $newNumber);
    }

    # Archive the most recent log file
    if (-e $logFile) {
        rename($logFile, $logFile . '.1');
    }
}

unless (-e $logPath) {
    my $r = `mkdir -p $logPath`;
    die $r if ($r);
}

# Handle any shell expansion, such as ~, in the path
my $realOutputPath = `cd $outputPath;pwd`;
chomp $realOutputPath;

our $outputFile = $realOutputPath . '/' . $outputFileName;

if (-e $outputFile) {
    unlink $outputFile;
}

my $ua = LWP::UserAgent->new();

our $token = '';
our $tokenLastUpdate = 0;

checkMUAuthenticationToken();

my $request;
my $response;


# retrieve all the account groups from CAM
$request = HTTP::Request->new('GET', $server .
    '/config/v1/EmailList?format=list&category=AccountDetails'. '&token=' . $token);

$request->header('Content-Type', 'application/json');

my $try = 0;
while (++$try < 3) {
    $response = $ua->simple_request($request);
    last if ($response->is_success());
    sleep 5;
}

unless ($response->is_success()) {
    exitWithError('message' => 'Failed to get account details from CAM', 'request' => $request, 'response' => $response);
}

my $groupList = extractResponseData('exitOnError' => 0, 'response' => $response, 'request' => $request);


my $count = 0;
my @emailRESTList;
my $emailREST;
my $i = 0;
my $responseCount = 0;

# open output file in append mode to write import IDs
# format will be accountType:importID1,importID2 so on
open OUT, ">>$outputFile" or die "Couldn't open $outputFile: $!";

# For each account type, get the LDAP entries and post it to Email Service

my ($groupLabel, $groupDetail);

GROUPLIST: while (($groupLabel, $groupDetail) = each %{ $groupList} ) {
    print OUT "$groupLabel:";

    my @detailsArray = split /:/, $groupDetail;

    unless (defined $detailsArray[2]) {
        exitWithError('message' => 'Affiliations of the group '. $groupLabel .' are not defined.');
    }
    my @groupAffiliations = split /,/, $detailsArray[2];


    my @importIDArray;
    $responseCount = 0;

    # Start LDAP connection
    my $ldapConnection = Net::LDAPS->new($ldapServer, 'timeout' => 15, 'version' => 3)
        or print "$ldapServer: $@\n";

    $result = $ldapConnection->bind(
        'dn' => $ldapBindDN, 'password' => $ldapBindPasswd
    );

    if ($result->code() != 0) {
        print $result->error() . "\n";
        exit;
    }
    my $page = Net::LDAP::Control::Paged->new(size => 500);
    my $cookie;

    my $filter = '(|';
    foreach my $affiliation (@groupAffiliations)
    {
        $filter .= '(muohioeduprimaryaffiliationcode='. $affiliation .')';
    }
    $filter .= ')';

    my @searchArgs = (
        'base'   => $ldapBase,
        # 'filter' => "(|(uid=tepeds)(uid=gengx))",
        'filter' => $filter,
        'attrs' => [
            'uid',
        ],
        'control' => [ $page ],
    );

    LDAP: while (my $pagedSearch = $ldapConnection->search(@searchArgs)) {

        $i = 0;
        @emailRESTList =();

        if ($pagedSearch->code) {
            print "LDAP Error: " . $pagedSearch->error . "\n";
        } elsif ($pagedSearch->count) {

            # Get number of records from LDAP. Number of record is equal to size mentioned above.
            $count = $count + $pagedSearch->count;
            foreach my $entry ($pagedSearch->entries()) {
                $emailREST = {};

                my $uid = $entry->get_value('uid');
                next if ($uid =~ /^rtr/);

                $emailREST->{uid} = $uid;
                $emailREST->{emailAddress} = $uid.'@miamioh.edu';
                $emailRESTList[$i] = $emailREST;
                $i++;

            }

            # PUT request to REST Email service, so that it can posted to EMMA
            checkMUAuthenticationToken();

            my $request;
            my $response;

            $request = HTTP::Request->new('PUT', $server .
                '/emailList/email/v1?accountType='.$groupLabel.'&token=' . $token);


            $request->content(to_json(\@emailRESTList));
            $request->header('Content-Type', 'application/json');

            my $try = 0;
            while (++$try < 3) {
                $response = $ua->simple_request($request);
                last if ($response->is_success());
                sleep 5;
            }

            unless ($response->is_success()) {

                logEntry('message' => 'Failed to update email List: '.$groupLabel);
                logEntry('message' => $request->as_string());
                logEntry('message' => $response->as_string());
                next GROUPLIST;

            }

            my $updateResults = extractResponseData('exitOnError' => 0, 'response' => $response, 'request' => $request);

            if( $updateResults) {
                $importIDArray[$responseCount++] =  $updateResults->{'emmaImportID'};
                logEntry('message'=> 'Updated Email List: '.$groupLabel);
            }

        }
        #print Dumper(@emailRESTList);
        #print Dumper(@importIDArray);

        # handle next search page
        my ($resp) = $pagedSearch->control(LDAP_CONTROL_PAGED)
            or die "Page error: $!";
        $cookie = $resp->cookie or last;
        $page->cookie($cookie);
    }

    # be nice to the server and stop the search if we still have a cookie
    if ($cookie) {
        $page->cookie($cookie);
        $page->size(0);
        $ldapConnection->search(@searchArgs);
    }

    $ldapConnection->unbind;
    # Assign list of import IDs to account type hash key
    #$putResponse{$acctType} = [@importIDArray];
    print OUT join( ',', @importIDArray );
    print OUT "\n";

}

close OUT;

sub extractResponseData {
    my $params = { @_ };

    my $request = $params->{'request'};
    my $response = $params->{'response'};
    my $exitOnError = defined($params->{'exitOnError'}) ? $params->{'exitOnError'} : 1;

    unless ($response->code == 200) {
        my $error = 'Request for service failed';

        $error = 'Authentication failed' if ($response->code == 401);

        if ($exitOnError) {
            exitWithError('message' => $error, 'request' => $request, 'response' => $response);
        } else {
            logEntry('message' => $error, 'request' => $request, 'response' => $response);
        }
    }

    my $payload = {};

    eval(q#$payload = from_json(${$response->content_ref()})#);

    if ($@) {
        exitWithError('message' => 'Failed to parse response ' . $@, 'request' => $request, 'response' => $response);
    }

    unless (defined($payload->{'data'})) {
        exitWithError('message' => 'Email response contains no data element', 'request' => $request, 'response' => $response);
    }

    return $payload->{'data'};
}

sub exitWithError {
    my $params = { @_ };

    my $message = $params->{'message'} || 'An unknown error occurred';

    print "$message\n";
    print $params->{'request'}->as_string() if ($params->{'request'});
    print $params->{'response'}->as_string() if ($params->{'response'});
    logEntry('message' => $message);
    logEntry('message' => $params->{'request'}->as_string()) if ($params->{'request'});
    logEntry('message' => $params->{'response'}->as_string()) if ($params->{'response'});
    exit 1;
}

sub logEntry {
    my $params = { @_ };

    my $message = $params->{'message'} || '';

    if ($message) {
        my $time = scalar localtime();
        print "[$time] $message\n" if ($VERBOSE);
        open LOG, ">>$logFile" or die "Couldn't open $logFile: $!";
        print LOG "[$time] $message\n";
        close LOG;
    }
}

sub checkMUAuthenticationToken {
    if ($token && time() < $tokenLastUpdate + (55 * 60)) {
        return 1;
    }

    getMUAuthenticationToken();
}

sub getMUAuthenticationToken {
    my $ua = LWP::UserAgent->new();

    my $request = HTTP::Request->new('POST', $server . '/authentication/v1');
    my $data = {
        'username' => $config{'username'},
        'password' => $config{'password'},
        'type' => 'usernamePassword'
    };

    $request->content(to_json($data));
    $request->header('Content-Type', 'application/json');

    my $response = $ua->simple_request($request);

    # Do not log the request since it has username/password
    my $tokenInfo = extractResponseData('response' => $response);

    $token = $tokenInfo->{'token'};
    $tokenLastUpdate = time();

}


