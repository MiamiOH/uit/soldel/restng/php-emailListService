#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

use LWP::UserAgent;
use JSON;
use Data::Dumper;

# 1. Declare our configuration variables, but do not define them. We
#    will do a defined() test later.
my $logPath;
my $logFileName;
my $keepLogs;
my $server;
my $username;
my $password;
my $configFile = '';
my $VERBOSE = 0;
my $help = 0;
my $inputPath;
my $inputFileName;
my $notificationAddress;

# 2. Read any options provided on the command line into our configuration
#    variables. This will cause those provided to be defined.
my $result = GetOptions(
  'log_dir=s' => \$logPath,
  'log=s' => \$logFileName,
  'log_history=s' => \$keepLogs,
  'config=s' => \$configFile,
  'verbose' => \$VERBOSE,
  'help' => \$help,
  'input_dir=s' => \$inputPath,
  'input_file=s' => \$inputFileName,
  'notifications=s' => \$notificationAddress,
  );

# 3. Document the command line options (and by extension the config file).
if ($help) {
  print <<EOP;

Call notification service for a list of recipients

Options:

           log_dir = path to log file
               log = log file name
       log_history = number of log files to keep
            config = path to config file
           verbose = verbose output
              help = display this help
         input_dir = path to input file
        input_file = input file name
     notifications = email address for notifications

EOP
}

# 4. Read the config file if provided.
our %config;
if (-e $configFile) {
  open CONFIG, $configFile or die "Couldn't open $configFile: $!";
  while (<CONFIG>) {
    chomp;
    next unless ($_);
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
  }
  close CONFIG;
}

# 5. Set up configuration from the config file, allowing override by
#    command line options if provided.
$logPath = $config{'log_dir'}
  if (!defined($logPath) && exists($config{'log_dir'}));
$logFileName = $config{'responseLog'}
  if (!defined($logFileName) && exists($config{'log'}));
$keepLogs = $config{'log_history'}
  if (!defined($keepLogs) && exists($config{'log_history'}));
$server = $config{'server'}
  if (!defined($server) && exists($config{'server'}));
$username = $config{'username'}
  if (exists($config{'username'}));
$password = $config{'password'}
  if (exists($config{'password'}));
$inputPath = $config{'input_dir'}
  if (!defined($inputPath) && exists($config{'input_dir'}));
$inputFileName = $config{'input_file'}
  if (!defined($inputFileName) && exists($config{'input_file'}));
$notificationAddress = $config{'notifications'}
  if (!defined($notificationAddress) && exists($config{'notifications'}));


# 6. Apply default configuration for any missing items.
$logPath = '/var/logs' unless (defined($logPath));
$logFileName = 'emma_get_response_log.txt' unless (defined($logFileName));
$keepLogs = 0 unless (defined($keepLogs));
$server = '' unless (defined($server));
$username = '' unless (defined($username));
$password = '' unless (defined($password));
$inputPath = '' unless (defined($inputPath));
$inputFileName = '' unless (defined($inputFileName));

unless ($server) {
  print "No valid Miami server provided in command line or config file\n";
  exit 1;
}

unless ($username) {
  print "No valid Miami username provided in command line or config file\n";
  exit 1;
}

unless ($password) {
  print "No valid Miami password provided in command line or config file\n";
  exit 1;
}

unless ($inputPath) {
  print "No valid input file path is provided in command line or config file\n";
  exit 1;
}

unless ($inputFileName) {
  print "No valid input file name provided in command line or config file\n";
  exit 1;
}

unless (-e $logPath) {
  my $r = `mkdir -p $logPath`;
  die $r if ($r);
}

# Handle any shell expansion, such as ~, in the path
my $realLogPath = `cd $logPath;pwd`;
chomp $realLogPath;

our $logFile = $realLogPath . '/' . $logFileName;

# Remove old logs
opendir(LOGDIR, $realLogPath) or die "Couldn't open $realLogPath: $!";
while (my $file = readdir(LOGDIR)) {
  next unless ($file =~ /^$logFileName\.(\d+)/);
  unlink($realLogPath . '/' . $file) if ($1 >= $keepLogs);
}
closedir(LOGDIR);

if ($keepLogs) {
  # Increment exist logs
  for (my $i = $keepLogs; $i >= 1; $i--) {
    my $newNumber = $i + 1;
    rename($logFile . '.' . $i, $logFile . '.' . $newNumber);
  }

  # Archive the most recent log file
  if (-e $logFile) {
    rename($logFile, $logFile . '.1');
  }
}

# Handle any shell expansion, such as ~, in the path
my $realInputPath = `cd $inputPath;pwd`;
chomp $realInputPath;

our $inputFile = $realInputPath . '/' . $inputFileName;

our $token = '';
our $tokenLastUpdate = 0;

checkMUAuthenticationToken();

my $ua = LWP::UserAgent->new();

my %putResponse; # key is accountType and value is list of Import IDs
my %getResponse;

# TODO Remove file once successfully processed
if (! -e $inputFile) {
    sendNotification(
            'toAddress' => $notificationAddress,
            'subject' => 'Email list import failed',
            'body' => "Import file $inputFile does not exist.",
        );
    exit 1;
}
open my $in, $inputFile or die "Couldn't open $inputFile: $!";

while(my $line = <$in>){
    chomp $line;
    my ($accountId,$importIDList) = split /:/, $line;
    # Verify if there are import IDs associated with account type in each line
    if($accountId && $importIDList) {
        $putResponse{$accountId} = [split /,/, $importIDList];
    }
}
close $in;

# GET request to Email REST to get members imported
# $resultString contains emma response details to be sent as part of email notification
my $resultString;
foreach my $acctType (keys %putResponse) {
    $resultString .= "For account type $acctType\n";
    my @responseArray;
    foreach my $importID (@{$putResponse{$acctType}}) {
        my $request;
        my $response;

        $resultString .= "\tImport ID $importID response:\n";
        checkMUAuthenticationToken();
        $request = HTTP::Request->new('GET', $server .
          '/emailList/email/v1?importID='. $importID . '&accountType='. $acctType . '&token=' . $token);

        $request->header('Content-Type', 'application/json');

        my $try = 0;
        while (++$try < 3) {
            $response = $ua->simple_request($request);
            last if ($response->is_success());
            sleep 5;
        }

        unless ($response->is_success()) {
            logError('message' => 'Failed to get member import details from Emma', 'request' => $request, 'response' => $response);
        }

        my $updateResults = extractResponseData('exitOnError' => 0, 'response' => $response, 'request' => $request);

        if ($updateResults->{'emmaResponse'}) {
           $resultString .= "\tmessage = Below are the list of emails added to active and error list.\n";
        } else {
            $resultString .= "\tmessage = $updateResults->{'message'}\n";
        }

        if($updateResults->{'emmaResponse'})
        {
            $resultString .= "\tAdded Email List:";

            if(! @{$updateResults->{'emmaResponse'}{'addedList'}})
            {
                $resultString .= "No Email Added\n";
            } else {
                $resultString .= "\n";
                my $addList;
                foreach $addList (@{$updateResults->{'emmaResponse'}{'addedList'}})
                {
                    $resultString .= "\t\tmember_id: $addList->{'member_id'}($addList->{'email'})\n";
                }
            }

            $resultString .= "\tError Email list:";
            if(! @{$updateResults->{'emmaResponse'}{'errorList'}})
            {
                $resultString .= "No Error Email\n";
            }else {
                $resultString .= "\n";
                my $errorList;
                foreach $errorList (@{$updateResults->{'emmaResponse'}{'errorList'}})
                {
                    $resultString .= "\t\tmember_id: $errorList->{'member_id'}\n";
                }
            }

        }
        $resultString .= "\n";
        $updateResults->{'importID'} = $importID;

        push @responseArray, $updateResults;

    }
    $getResponse{$acctType} = \@responseArray;
}

logEntry('message' => $resultString);

sendNotification(
        'toAddress' => $notificationAddress,
        'subject' => 'Email list import complete',
        'body' => $resultString,
    );

sub sendNotification {
    my $params = { @_ };

    my $toAddress = $params->{'toAddress'} || '';
    my $fromAddress = $params->{'fromAddress'} || 'noreply@miamioh.edu';
    my $priority = $params->{'priority'} || 2;
    my $subject = $params->{'subject'} || '';
    my $body = $params->{'body'} || '';

    if ($toAddress) {
        checkMUAuthenticationToken();

        my $request = HTTP::Request->new('POST', $server .
          '/notification/v1/email/message?token=' . $token);

        $request->content(to_json({
            'dataType' => 'model',
            'data' => {
                'toAddr' => $toAddress,
                'fromAddr' => $fromAddress,
                'priority' => $priority,
                'subject' => $subject,
                'body' => $body,
            },
        }));

        $request->header('Content-Type', 'application/json');

        my $response;
        my $try = 0;
        while (++$try < 3) {
            $response = $ua->simple_request($request);
            last if ($response->is_success());
            sleep 5;
        }

        unless ($response->is_success()) {
            exitWithError('message' => 'Failed to update email list', 'request' => $request, 'response' => $response);
        }

        my $notificationResults = extractResponseData('exitOnError' => 0, 'response' => $response, 'request' => $request);

        print Dumper($notificationResults);
    } else {
        print <<EOP;
From:     $fromAddress
Priority: $priority
Subject:  $subject

$body
EOP

    }
    return 1;
}

sub extractResponseData {
  my $params = { @_ };

  my $request = $params->{'request'};
  my $response = $params->{'response'};
  my $exitOnError = defined($params->{'exitOnError'}) ? $params->{'exitOnError'} : 1;

  unless ($response->is_success()) {

    my $error = 'Request for service failed';

    $error = 'Authentication failed' if ($response->code == 401);

    if ($exitOnError) {
      exitWithError('message' => $error, 'request' => $request, 'response' => $response);
    } else {
      logEntry('message' => $error, 'request' => $request, 'response' => $response);
    }
  }

  my $payload = {};

  eval(q#$payload = from_json(${$response->content_ref()})#);

  if ($@) {
    exitWithError('message' => 'Failed to parse response ' . $@, 'request' => $request, 'response' => $response);
  }

  unless (defined($payload->{'data'})) {
    exitWithError('message' => 'Email response contains no data element', 'request' => $request, 'response' => $response);
  }

  return $payload->{'data'};
}

sub logError {
  my $params = { @_ };

  my $message = $params->{'message'} || 'An unknown error occurred';

  print "$message\n";
  print $params->{'request'}->as_string() if ($params->{'request'});
  print $params->{'response'}->as_string() if ($params->{'response'});
  logEntry('message' => $message);
  logEntry('message' => $params->{'request'}->as_string()) if ($params->{'request'});
  logEntry('message' => $params->{'response'}->as_string()) if ($params->{'response'});

}

sub logEntry {
  my $params = { @_ };

  my $message = $params->{'message'} || '';

  if ($message) {
    my $time = scalar localtime();
    print "[$time] $message\n" if ($VERBOSE);
    open LOG, ">>$logFile" or die "Couldn't open $logFile: $!";
    print LOG "[$time] $message\n";
    close LOG;
  }
}

sub exitWithError {
  my $params = { @_ };

  my $message = $params->{'message'} || 'An unknown error occurred';

  print "$message\n";
  print $params->{'request'}->as_string() if ($params->{'request'});
  print $params->{'response'}->as_string() if ($params->{'response'});
  logEntry('message' => $message);
  logEntry('message' => $params->{'request'}->as_string()) if ($params->{'request'});
  logEntry('message' => $params->{'response'}->as_string()) if ($params->{'response'});
  exit 1;
}

sub checkMUAuthenticationToken {
  if ($token && time() < $tokenLastUpdate + (55 * 60)) {
    return 1;
  }

  getMUAuthenticationToken();
}

sub getMUAuthenticationToken {
  my $ua = LWP::UserAgent->new();

  my $request = HTTP::Request->new('POST', $server . '/authentication/v1');
  my $data = {
      'username' => $config{'username'},
      'password' => $config{'password'},
      'type' => 'usernamePassword'
  };

  $request->content(to_json($data));
  $request->header('Content-Type', 'application/json');

  my $response = $ua->simple_request($request);

  # Do not log the request since it has username/password
  my $tokenInfo = extractResponseData('response' => $response);

  $token = $tokenInfo->{'token'};
  $tokenLastUpdate = time();

}


